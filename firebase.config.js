import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";

const firebaseConfig = {
  apiKey: "AIzaSyAd8BGbpw-84b86IVdzNcPF1xkq22yBa5g",
  authDomain: "meal-to-go-8b47e.firebaseapp.com",
  projectId: "meal-to-go-8b47e",
  storageBucket: "meal-to-go-8b47e.appspot.com",
  messagingSenderId: "600976374594",
  appId: "1:600976374594:web:0aaafaa095a0186d9026db",
  measurementId: "G-8YD1672KGF",
};

const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);
