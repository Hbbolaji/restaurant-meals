import React, { useState, createContext, useEffect, useContext } from "react";
import { mockImages } from "../../services/restaurant/mock";

import { restaurantsRequest } from "./restaurants.service";
import camelize from "camelize";
import { LocationContext } from "../location/location.context";

export const RestaurantsContext = createContext();

const transformRestaurant = (result = []) => {
  return result.map((res) => {
    res.isOpenNow = res.opening_hours && res.opening_hours.open_now;
    res.isClosedTemporarily = res.bussiness_status === "CLOSED_TEMPORARILY";
    res.address = res.vicinity;
    res.photos = res.photos.map((p) => {
      return mockImages[Math.ceil(Math.random() * (mockImages.length - 1))];
    });
    return camelize(res);
  });
};

const RestaurantsContextProvider = ({ children }) => {
  const [restaurants, setRetaurants] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);
  const { location } = useContext(LocationContext);

  const retrieveRestaurants = (loc) => {
    setIsLoading(true);
    setRetaurants([]);
    setTimeout(() => {
      restaurantsRequest(loc.latLng)
        .then((res) => {
          setRetaurants(transformRestaurant(res.results));
          setIsLoading(false);
        })
        .catch((err) => {
          setError(err);
          setIsLoading(false);
        });
    }, 1000);
  };

  useEffect(() => {
    if (location) {
      retrieveRestaurants(location);
    }
  }, [location]);
  return (
    <RestaurantsContext.Provider value={{ restaurants, isLoading, error }}>
      {children}
    </RestaurantsContext.Provider>
  );
};

export default RestaurantsContextProvider;
