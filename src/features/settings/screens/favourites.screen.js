import { Text } from "react-native";
import React, { useContext } from "react";

import SafeArea from "../../../components/utility/safeArea.component";
import { FavouritesContext } from "../../../services/favourites/favourites.context";
import styled from "styled-components/native";
import { RestaurantList } from "../../Restaurants/components/restaurant-list.styles";
import { TouchableOpacity } from "react-native-gesture-handler";
import Spacer from "../../../components/spacer/spacer.component";
import RestaurantInfoCard from "../../Restaurants/components/restaurant-info-card.component";

const NoFavouritesArea = styled(SafeArea)`
  flex:1
  align-items: center;
  justify-content: center;
`;

const FavouritesScreen = ({ navigation }) => {
  const { favourites } = useContext(FavouritesContext);

  return favourites.length ? (
    <SafeArea>
      <RestaurantList
        data={favourites}
        renderItem={({ item }) => {
          return (
            <TouchableOpacity
              onPress={() =>
                navigation.navigate("RestaurantDetail", { restaurant: item })
              }
            >
              <Spacer position="bottom" size="large">
                <RestaurantInfoCard restaurant={item} />
              </Spacer>
            </TouchableOpacity>
          );
        }}
        keyExtractor={(item) => item.name}
      />
    </SafeArea>
  ) : (
    <NoFavouritesArea>
      <Text center>No Favourites yet</Text>
    </NoFavouritesArea>
  );
};

export default FavouritesScreen;
