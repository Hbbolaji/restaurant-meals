import { ScrollView } from "react-native";
import React, { useState } from "react";
import { List } from "react-native-paper";

import RestaurantInfoCard from "../components/restaurant-info-card.component";
import styled from "styled-components/native";

const Container = styled.View`
  padding: 10px;
`;

const ListIcon = (props, icon) => <List.Icon {...props} icon={icon} />;

const RestaurantDetailScreen = ({ route }) => {
  const [breakfastExpanded, setBreakfastExpanded] = useState(false);
  const [lunchExpanded, setLunchExpanded] = useState(false);
  const [dinnerExpanded, setDinnerExpanded] = useState(false);
  const [drinks, setDrinks] = useState(false);
  const { restaurant } = route.params;
  return (
    <Container>
      <RestaurantInfoCard restaurant={restaurant} />
      <ScrollView>
        <List.Accordion
          title="BreakFast"
          left={(props) => ListIcon(props, "bread-slice")}
          expanded={breakfastExpanded}
          onPress={() => setBreakfastExpanded((prev) => !prev)}
        >
          <List.Item title="Eggs Benedict" />
          <List.Item title="Classic Breakfast" />
        </List.Accordion>
        <List.Accordion
          title="Lunch"
          left={(props) => ListIcon(props, "hamburger")}
          expanded={lunchExpanded}
          onPress={() => setLunchExpanded((prev) => !prev)}
        >
          <List.Item title="Burger w/ Fries" />
          <List.Item title="Steak Sandwich" />
          <List.Item title="Mushroom Soup" />
        </List.Accordion>
        <List.Accordion
          title="Dinner"
          left={(props) => ListIcon(props, "food-variant")}
          expanded={dinnerExpanded}
          onPress={() => setDinnerExpanded((prev) => !prev``)}
        >
          <List.Item title="Spaghetti Bolognese" />
          <List.Item title="Veal Cutlet with Chicken Mushroom Rotini" />
          <List.Item title="Steak Frites" />
        </List.Accordion>
        <List.Accordion
          title="Drinks"
          left={(props) => ListIcon(props, "cup")}
          expanded={drinks}
          onPress={() => setDrinks((prev) => !prev)}
        >
          <List.Item title="Coffee" />
          <List.Item title="Tea" />
          <List.Item title="Modelo" />
          <List.Item title="Coke" />
          <List.Item title="Fanta" />
        </List.Accordion>
      </ScrollView>
    </Container>
  );
};

export default RestaurantDetailScreen;
