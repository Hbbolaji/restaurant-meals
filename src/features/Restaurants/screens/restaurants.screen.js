import { TouchableOpacity } from "react-native";
import React, { useContext, useState } from "react";
import styled from "styled-components/native";
import { ActivityIndicator, MD2Colors } from "react-native-paper";

import RestaurantInfoCard from "../components/restaurant-info-card.component";
import SafeArea from "../../../components/utility/safeArea.component";
import Spacer from "../../../components/spacer/spacer.component";
import Search from "../components/search.component";

import { FavouritesContext } from "../../../services/favourites/favourites.context";
import { RestaurantsContext } from "../../../services/restaurant/restaurants.context";
import FavouritesBar from "../../../components/favourites/favourites-bar.component";
import { RestaurantList } from "../components/restaurant-list.styles";
import FadeInView from "../../../components/animations/fade.animation";

const Loading = styled(ActivityIndicator)`
  flex: 1;
`;

const RestaurantsScreen = ({ navigation }) => {
  const { restaurants, isLoading } = useContext(RestaurantsContext);
  const { favourites } = useContext(FavouritesContext);
  const [isToggled, setIsToggled] = useState(false);

  return (
    <SafeArea>
      <Search
        isFavouritesToggled={isToggled}
        onFavouritesToggle={() => setIsToggled(!isToggled)}
      />
      {isToggled && (
        <FavouritesBar
          favourites={favourites}
          onNavigate={navigation.navigate}
        />
      )}
      {isLoading && <Loading animating={true} color={MD2Colors.green500} />}
      <RestaurantList
        data={restaurants}
        renderItem={({ item }) => {
          return (
            <FadeInView>
              <TouchableOpacity
                onPress={() =>
                  navigation.navigate("RestaurantDetail", { restaurant: item })
                }
              >
                <Spacer position="bottom" size="large">
                  <RestaurantInfoCard restaurant={item} />
                </Spacer>
              </TouchableOpacity>
            </FadeInView>
          );
        }}
        keyExtractor={(item) => item.name}
      />
    </SafeArea>
  );
};

export default RestaurantsScreen;
