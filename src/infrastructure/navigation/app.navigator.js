import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { Ionicons } from "@expo/vector-icons";

import RestaurantsNavigator from "./restaurant.navigator";
import MapScreen from "../../features/map/screens/map.screen";
import RestaurantsContextProvider from "../../services/restaurant/restaurants.context";
import LocationContextProvider from "../../services/location/location.context";
import FavouritesContextProvider from "../../services/favourites/favourites.context";
import SettingsNavigator from "./settings.navigator";

const TAB_ICON = {
  RestaurantStack: "md-restaurant",
  SettingsStack: "md-settings",
  Map: "md-map",
};

const tabBarIcon = (route, color, size) => {
  return <Ionicons name={TAB_ICON[route.name]} size={size} color={color} />;
};

const AppNavigator = () => {
  const Tab = createBottomTabNavigator();
  return (
    <FavouritesContextProvider>
      <LocationContextProvider>
        <RestaurantsContextProvider>
          <Tab.Navigator
            screenOptions={({ route }) => ({
              tabBarIcon: ({ color, size }) => tabBarIcon(route, color, size),
              headerShown: false,
              tabBarActiveTintColor: "tomato",
              tabBarInactiveTintColor: "gray",
            })}
          >
            <Tab.Screen
              name="RestaurantStack"
              component={RestaurantsNavigator}
            />
            <Tab.Screen name="Map" component={MapScreen} />
            <Tab.Screen name="SettingsStack" component={SettingsNavigator} />
          </Tab.Navigator>
        </RestaurantsContextProvider>
      </LocationContextProvider>
    </FavouritesContextProvider>
  );
};

export default AppNavigator;
